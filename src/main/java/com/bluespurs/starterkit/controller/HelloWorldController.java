package com.bluespurs.starterkit.controller;

//LOG
import com.bluespurs.starterkit.domain.Json;
import com.bluespurs.starterkit.domain.Product;
import com.google.gson.JsonObject;
import com.oracle.javafx.jmx.json.impl.JSONMessages;
import org.apache.catalina.connector.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
//REST
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
//SERVICE
import com.bluespurs.starterkit.service.SearchService;

//import org.json.JSONException;
//import org.json.JSONObject;

@RestController
public class HelloWorldController {
    public static final String INTRO = "The Bluespurs Interview Starter Kit is running properly.";
    public static final Logger log = LoggerFactory.getLogger(HelloWorldController.class);

    @Autowired
    public HelloWorldController(SearchService searchService) {
        this.searchService = searchService;
    }
    //
    private final SearchService searchService;

    /**
     * The index page returns a simple String message to indicate if everything is working properly.
     * The method is mapped to "/" as a GET request.
     */
    @RequestMapping("/")
    public String helloWorld() {
        log.info("Visiting index page");
        return INTRO;
    }

    @RequestMapping("/product")
    public String productInformation() {
        log.info("Visiting product page");
        return "0";
    }

    @RequestMapping("/product/searchCheapestPrice" )
    public Json getCheapestPriceProduct(@RequestParam("productName") String productName){

        Product responseProduct = new Product();
        Json jsonMessage = new Json();
        if(productName!=null){//continue process

            //call service check price and get response product
            responseProduct = searchService.getCheapestPrice(productName);
            jsonMessage.setJsonData(responseProduct);
            //jsonMessage.setErrorMessage("No Errors Message");
            //comparie price
        }
        else{ // handel
            //jsonMessage.setErrorMessage("No product Name");
        }

        return jsonMessage;
    }


}
