package com.bluespurs.starterkit.service;

/**
 * Created by jujai on 2016-05-26.
 */
import com.bluespurs.starterkit.domain.Product;

public interface SearchService {

     Product getCheapestPrice(String productName);

}
