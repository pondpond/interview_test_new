package com.bluespurs.starterkit.service;


import com.bluespurs.starterkit.domain.Product;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.util.JSONPObject;
import com.fasterxml.jackson.databind.util.TypeKey;
import jdk.nashorn.internal.parser.JSONParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.w3c.dom.ProcessingInstruction;

import java.util.Iterator;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Map;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;


/**
 * Created by jujai on 2016-05-26.
 */
@Service
public class SearchServiceImpl implements SearchService {

    private static final Logger LOGGER = LoggerFactory.getLogger(SearchServiceImpl.class);
    private static final int URL_NUMB = 2;

    public Product getCompareLowestPrice(Object javaObject) {
        Product LowestProduct = new Product();
        return LowestProduct;
    }


    public Product JsonStringToJavaObject(String jsonString) {

        ObjectMapper mapper = new ObjectMapper();
        //create tree from JSON
        Product product = new Product();
        try{
        JsonNode rootNode = mapper.readTree(jsonString);
        //JsonNode productsNode = rootNode.path("products");
        /*
        Iterator<JsonNode> iterator = productsNode.get();
        while (iterator.hasNext()) {
            JsonNode products = iterator.next();
        }
        */

        } catch (JsonParseException e) {
            e.printStackTrace();
        } catch (JsonMappingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        //System.out.println(staff);
        return product;
    }

    @Override
    public Product getCheapestPrice(String productName) {
        Product product = new Product();
        //product.setProductName(productName);
        LOGGER.info("Get cheapest price from " + productName);
        String result = "";
        String[] URL = new String[URL_NUMB-1];
        Product[] cheapest = new Product[1];
        try {
            productName = "ipad";
            String walmartApi = "http://api.walmartlabs.com/v1/search?query="+productName+"&format=json&apiKey=rm25tyum3p9jm9x9x7zxshfa";
            String bestbuyApi = "https://api.bestbuy.com/v1/products((search="+productName+"))?apiKey=pfe9fpy68yg28hvvma49sc89&callback=JSON_CALLBACK&format=json";
            String[] apiUrl = new String[]{walmartApi,bestbuyApi};

            String jsonString = "";
            //return "Please put the name of product after path.";

            for (int i = 1; i < apiUrl.length; i++) {
                URL url = new URL(apiUrl[i].toString());

                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setRequestMethod("GET");
                conn.setRequestProperty("Accept", "application/json");
                // Check
                if (conn.getResponseCode() != 200) {
                    continue;
                    //throw new RuntimeException("Failed : HTTP error codeb : " + conn.getResponseCode());
                }
                BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));
                String output;
                System.out.println("Output from Server\n");
                while ((output = br.readLine()) != null) {
                    jsonString = jsonString+output;
                }


                //product.setProductName(jsonString);
                //Object javaObject = JsonStringToJavaObject(jsonString);
                //Product productLowPrice = getCompareLowestPrice(javaObject);
                //product.setProductName(productt.getName().toString());

                conn.disconnect();
                //cheapest[i] = productLowPrice;
            }

        } catch (MalformedURLException e) {

            e.printStackTrace();

        } catch (IOException e) {

            e.printStackTrace();

        }

        if(cheapest[0].getBestPrice() > cheapest[1].getBestPrice()){
            product = cheapest[1];
        }
        else {
            product = cheapest[0];
        }

        return product;

    }


}
