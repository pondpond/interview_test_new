package com.bluespurs.starterkit.domain;

/**
 * Created by jujai on 2016-05-27.
 */
public class Json {
    private String errorMessage;
    public String getErrorMessage() {
        return errorMessage;
    }
    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    private Product jsonData ;
    public Product getJsonData() {
        return jsonData;
    }
    public void setJsonData(Product jsonData) {
        this.jsonData = jsonData;
    }


}
