package com.bluespurs.starterkit.domain;

import javax.persistence.*;
/**
 * Created by jujai on 2016-05-26.
 */
public class Product {



    private long sku;
    public long getSku() {
        return sku;
    }
    public void setSku(long id) {
        this.sku = id;
    }

    private String name;
    public String getName(){
        return name;
    }
    public void setName(String name){
        this.name = name;
    }

    private  String productName;
    public String getProductName() {
        return productName;
    }
    public void setProductName(String productName) {
        this.productName = productName;
    }


    private  long bestPrice;
    public long getBestPrice() {
        return bestPrice;
    }
    public void setBestPrice(long bestPrice) {
        this.bestPrice = bestPrice;
    }

    private  String currency;
    public String getCurrency() {
        return currency;
    }
    public void setCurrency(String currency) {
        this.currency = currency;
    }

    private  String location;
    public String getLocation() {
        return location;
    }
    public void setLocation(String location) {
        this.location = location;
    }
}
